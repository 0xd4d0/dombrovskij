# dombrovskij.de

This is the repo for my personal website at [dombrovskij.de](https://dombrovskij.de)

# Mods

build.sh is shamelessly stolen from [lukasec](https://lukasec.ch) as well as the CSS. I just really like the design. But it appears the original author deleted the repository. :(

# Thanks

* lukasec for creating the build.sh script
* [Dark-Matter7232](https://github.com/Dark-Matter7232/) for modifying [ssg6](https://github.com/Dark-Matter7232/ssg6)
* [Roman Zolotarev](https://romanzolotarev.com/) for creating [ssg](https://romanzolotarev.com/ssg.html)
* Font is from [dtinth](https://github.com/dtinth/comic-mono-font/)
