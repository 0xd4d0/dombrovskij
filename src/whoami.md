---
title: whoami
date: 2022-07-09
---
<h1 style="display: none;">whoami</h1>

# whoami
I am a russian born dude who moved to Germany with his family at the age of 10. I like hacking things for fun (and a living). 
I also play bass, compose music, love to spend time in nature away from technology and sometimes also pretend to be a wizard or barbarian in a good old d&d session. At the end of 2023 I also became a father. 

# experience
I have been working as a penentration tester for more than five years. During this time I had the opportunity to meet clients from various industries ranging from finance to manufacturing as well as municipality companies and critical infrastructure. I have experience in identifying and exploitig vulnerabilities in web applications, linux based and Windows systems (thin- and thick-clients), network access control systems and WiFi. Part of my job is also knowledge transfer. I organize and facilitate workshops on OWASP top 10 security risks in web applications and APIs. 
There is more I can talk about, but I think if would be best to refer you to my <a href='https://gitlab.com/0xd4d0/dombrovskij/-/blob/main/dst/cv/cv_daniil_dombrovskij_en.pdf?ref_type=heads'>CV</a>. 

Contact Oldschool   [hello AT dombrovskij DOT de]<br>
Contact Newschool   [[Gitlab](https://gitlab.com/0xd4d0), <a rel="me" href="https://23.social/@d4ny4">Mastodon</a>, [Linkedin](https://www.linkedin.com/in/dombrovskij/)]

# this site
this is my online presence. it security professionals (and those aspiring to be) are kind of exptected to have an online presence, so here i am!
