---
title: impressum
date: 2024-02-10
---
<h1 style="display: none;">legal</h1>

# impressum
Angaben gemäß § 5 TMG:

E-Mail:
[hello AT dombrovskij DOT de]

Text, Gestaltung, Umsetzung, Entwicklung:
[Daniil Dombrovskij]

Rechtliche Hinweise zum Urheberrecht:
[Das Layout der Homepage, die verwendeten Grafiken sowie die sonstigen Inhalte sind urheberrechtlich geschützt.]

Die Datenschutzerklärung finden Sie <a href='/./gdpr.hmtl'>hier</a>

Contact Oldschool   [hello AT dombrovskij DOT de]<br>
Contact Newschool   [[Gitlab](https://gitlab.com/0xd4d0), [Twitter](https://twitter.com/0xd4d0), <a rel="me" href="https://23.social/@d4ny4">Mastodon</a>, [Linkedin](https://www.linkedin.com/in/dombrovskij/), [Xing](https://www.xing.com/profile/Daniil_Dombrovskij/cv)]
