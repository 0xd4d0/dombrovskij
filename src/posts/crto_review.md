---
title:  a review of the red team operator certification
date: 2025-02-03
description: i recently passed the crto exam and this is my review
---

## motivation
  
i wanted to expand my knowledge in active directory hacking and the crto course seemed like the ideal way to do that. sure you can spin up a lab and play around with it but i prefer a structured approach which crto provided.

## the course

### TL;DR
  
5/7 perfect score would recommend. affordable, very enjoyable, very fair exam, lot of things to learn and many more things left to learn (i.e. in RTO II)
here is my [shiny certificate](https://eu.badgr.com/backpack/badges/679d80bf1ff27331ecf1ac10), looking forward to RTO II. overwall basic concepts, however, general it security knowledge is required since many concepts are not explained in detail. if you have 1-2 years either personall or professional experience then you are good to go.

### course overview
  
i could just copy paste the syllabys from the [course's website](https://training.zeropointsecurity.co.uk/courses/red-team-ops) and leave at that. 
  
maybe i will just do that...
  
in short this is an introductory course to adversary simulation and red team operations. fancy speak for 'learn to hack companies'. 
you learn many interesting and fancy techniques to move around in a corporate active directory environment, how to exploit different misconfigurations to elevate your privileges, move around systems, gain domain dominance and even go beyond to other domains (no CVEs were harmed during the course and exam).
  
the course gives a few OPSEC (operational security or 'how to stay hidden') considerations, but it is not the main focus. this is what the RTO II is for.

#### preparation
  
i basically spent six month preparing for the exam. mainly because you really embrace the trendy #slowliving lifestyle when you get kids. do make no mistake i absolutely love it and would do it again, but you have to be aware that time management becomes very important. see my post [reflections](https://dombrovskij.de/posts/reflections.html) if you are interested in what it feels like to be a dad after one year. anyway, this is why i unironically woke up at 5am for the past few months. even on sundays (basically heresy in germany)! this allowed me to study very efficiently while the family was still asleep. after studying it was either work or spending time with family. 
in december i decided on an exam date so i can finally feel the pressure of an approaching deadline. 
  
overall i went thrice through the whole material. once without defender enabled as is described in the course and twice with defender enabled. you really should do this because in the exam defender is enabled. i did it this way because the second time i went through the course (first time with defender enabled) was around september and i had not set up an exam yet because the time was just not right due to external reasons. the third time was around december/january, again with defender enabled. yes, i probably bought too many lab extensions but \*shrug\*.

#### exam 
  
my exam was scheduled for jan 29th until feb 2nd. in total you have 48 hours to complete the exam. you may distribute them in these four days however you see fit. i started around 9am on thursday, set myself a timer until 10pm that day, continued on friday at around 9am again and was done by 1130am, so around 16 hours in total. right before lunch!
![my scoring](/img/scoring_crto.png)
the exam was *very* fair. hats off to rastamouse for creating such a pleasent experience. unlike some *other* certificates that are pure agony and despair. 48 hours sounds like a lot, and it is. mainly because you will run out of ideas loooong before you run out of time. i had a clear direction at every step of the exam, there were no rabbit holes *cough* like in other certifications *cough*. of course the solution was not presented to you on a *sliver* plate (got it? sliver! it's a c2... ok i'll stop), but i always had a clear target before me.

### now what? 
  
again, this was one of the most pleasent exams i attempted. i only got hungry for more knowledge in that area and this is exactly what i am going to do. i have a [goad](https://github.com/Orange-Cyberdefense/GOAD/) (vulnerable active directory lab) running on my personal proxmox and i want to compromise it using [sliver](https://sliver.sh/), stay tuned for the walkthrough for that! i will also buy the RTO II lab once i had some well deserved rest.
  
bye
