#!/bin/sh
# 
# author 
# lu|ka
# https://lukasec.ch
#
# based on
# https://github.com/Dark-Matter7232/ssg6
#
# original code by
# Roman Zolotarev <hi@romanzolotarev.com>
# https://rgz.ee/bin/ssg6
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above 
# copyright notice and this permission notice appear in all copies.  
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

main() {
	test -n "$1" || usage # source dir
	test -n "$2" || usage # destination dir
	test -n "$3" || usage # site name
	test -n "$4" || usage # site baseurl
	test -n "$5" || usage # site author
	test -n "$6" || usage # site description

	# only strings no dirs
	test -d "$1" || no_dir "$1"
	test -d "$2" || no_dir "$2"

	src=$(readlink_f "$1")
	dst=$(readlink_f "$2")

	IGNORE=$(
		if ! test -f "$src/.ssgignore"; then
			printf ' ! -path "*/.*"'
			return
		fi
		while read -r x; do
			test -n "$x" || continue
			printf ' ! -path "*/%s*"' "$x"
		done <"$src/.ssgignore"
	)

	# author, site description
	author="$5"
	site_description="$6"

	# files
	title="$3"

	h_file="$src/_header.html"
	f_file="$src/_footer.html"
	test -f "$f_file" && FOOTER=$(cat "$f_file") && export FOOTER
	test -f "$h_file" && HEADER=$(cat "$h_file") && export HEADER

	list_dirs "$src" |
		(cd "$src" && cpio -pdu --quiet "$dst")

	fs=$(
		if test -f "$dst/.files"; then
			list_affected_files "$src" "$dst/.files"
		else
			list_files "$1"
		fi
	)
  echo "$fs" | grep '\.md$' |

	if test -n "$fs"; then
		echo "$fs" | tee "$dst/.files"

		if echo "$fs" | grep -q '\.md$'
		then
      echo "$fs" | grep '\.md$' | generate_post_list "$src"
      if test -x "$(which pandoc 2> /dev/null)"
      then
        echo "$fs" | grep '\.md$' |
        render_md_files_pandoc "$src" "$dst" "$title" "$4"
      else
        echo "couldn't find pandoc"
        exit 3
      fi
		fi
    wait
    # Sort the post list by date using vim
    vim -N -u NONE -n \
      -c "set nomore" \
      -c ":g/SortByDate:/,/SortByDate/ s/$\n/@@@" \
      -c ":sort!" -c ":%s/@@@/\r/g" -c ":g/SortByDate/d" \
      -c ":wq" $src/index.html 2> /dev/null
    sed -i "1s/^/<h1 style=\"display: none;\">home<\/h1><h1>posts<\/h1>\n /" $src/index.html

    vim -N -u NONE -n \
      -c "set nomore" \
      -c ":g/SortByDate:/,/SortByDate/ s/$\n/@@@" \
      -c ":sort!" -c ":%s/@@@/\r/g" -c ":g/SortByDate/d" \
      -c ":wq" $src/posts.html 2> /dev/null
    sed -i "1s/^/<h1>posts<\/h1>\n/" $src/posts.html

	# only show latest 3 posts on frontpage
  	# get the line that starts the 4th entry
    line=$(awk '/<a / && ++n==4 {print NR}' $src/index.html)	
    head -$(($line-1)) $src/index.html> $src/index0.html
    mv $src/index0.html $src/index.html

		echo "$fs" | grep '\.html$' |
			render_html_files "$src" "$dst" "$title"

		echo "$fs" | grep -Ev '\.md$|\.html$' |
			(cd "$src" && cpio -pu --quiet "$dst")
	fi

	#printf '[*] ssg - ' >&2
	#print_status 'file, ' 'files, ' "$fs" >&2

	# sitemap
	base_url="$4"
	date=$(date +%Y-%m-%d)
	urls=$(list_pages "$src")

	test -n "$urls" &&
		render_sitemap "$urls" "$base_url" "$date" >"$dst/sitemap.xml"
	#print_status 'url' 'urls' "$urls" >&2
	#echo >&2

}

readlink_f() {
	file="$1"
	cd "$(dirname "$file")"
	file=$(basename "$file")
	while test -L "$file"; do
		file=$(readlink "$file")
		cd "$(dirname "$file")"
		file=$(basename "$file")
	done
	dir=$(pwd -P)
	echo "$dir/$file"
}

print_status() {
	test -z "$3" && printf 'no %s' "$2" && return

	echo "$3" | awk -v singular="$1" -v plural="$2" '
	END {
		if (NR==1) printf NR " " singular
		if (NR>1) printf NR " " plural
	}'
}

usage() {
	echo "usage: ${0##*/} src dst title base_url authoremail description" >&2
	exit 1
}

no_dir() {
	echo "${0##*/}: $1: No such directory" >&2
	exit 2
}

list_dirs() {
	cd "$1" && eval "find . -type d ! -name '.' ! -path '*/_*' $IGNORE"
}

list_files() {
	cd "$1" && eval "find . -type f ! -name '.' ! -path '*/_*' $IGNORE"
}

list_dependant_files() {
	e="\\( -name '*.html' -o -name '*.md' -o -name '*.css' -o -name '*.js' \\)"
	cd "$1" && eval "find . -type f ! -name '.' ! -path '*/_*' $IGNORE $e"
}

list_newer_files() {
	cd "$1" && eval "find . -type f ! -name '.' $IGNORE -newer $2"
}

has_partials() {
	grep -qE '^./_.*\.html$|^./_.*\.js$|^./_.*\.css$'
}

list_affected_files() {
	fs=$(list_newer_files "$1" "$2")

	if echo "$fs" | has_partials; then
		list_dependant_files "$1"
	else
		echo "$fs"
	fi	
}

render_html_files() {
	while read -r f; do
		render_html_file "$3" <"$1/$f" >"$2/$f"
	done
}

render_md_files_pandoc() {
	# create the RSS file at yoursite.com/feed.xml
	echo "[*] Creating RSS file"
	echo '<?xml version="1.0" encoding="UTF-8"?>' > "$dst/feed.xml"
	echo '<rss version="2.0">' >> "$dst/feed.xml"
	echo '<channel>' >> "$dst/feed.xml"
	echo "<title>"$3"</title>" >> "$dst/feed.xml" 
	echo "<link>"$4"</link>" >> "$dst/feed.xml"
	# somehow bash adds an 'n' to the string so we remove the last character
	echo "<description>${site_description%?}</description>" >> "$dst/feed.xml"
	echo "<lastBuildDate>$(date -R)</lastBuildDate>" >> "$dst/feed.xml"
  
	while read -r f
  		do
			echo "[*] Rendering $f"
			TITLE=$(grep -i title "$1/$f" | head -n1 | awk -F ": " '{ print $2 }')
			DESC=$(grep -i description "$1/$f" | head -n1 | awk -F ": " '{ print $2 }')
			IMAGE=$(grep -i image "$1/$f" | head -n1 | awk -F ": " '{ print $2 }')
			URI=$(echo "$2/${f%\.md}.html" | cut -d . -f 2)
			DATE=$(grep -i date "$1/$f" | head -n1 | awk -F ": " '{ print $2 }' | sed 's/;$//')
			pandoc --highlight-style pygments --toc "$1/$f" |
				render_html_file "$3" \
				> "$2/${f%\.md}.html" && \
			sed -i -e "/<title>/a <meta property=\"og:title\" content=\"$TITLE\" />\n\
			<meta property=\"og:description\" content=\"$DESC\" />\n\
			<meta property=\"og:type\" content=\"article\" />\n\
			<meta property=\"og:url\" content=\"$URI\" />\n\
			<meta property=\"og:image\" content=\"$4$IMAGE\" />\n\
			<meta name=\"twitter:image:src\" content=\"$4$IMAGE\" />\n\
			<meta name=\"title\" content=\"$TITLE\" />\n\
			<meta name=\"description\" content=\"$DESC\" />\n"\
			"$2/${f%\.md}.html" &
			# add each blog post to the RSS file while skipping some 
			case "$TITLE" in
				*whoami*) # skip whoami.html
					#echo '[*] Skipping whoami.md for the RSS feed'
					;;
				*404*) # skip 404.html
					#echo '[*] Skipping 404.md for the RSS feed'
					;;
				*)
					# the hour and minute of the date is fixed since inside the .md files only yyyy-MM-dd is specified
					#echo "[*] Adding $TITLE to the RSS feed"
					echo "<item><title>"$TITLE"</title>\
					<link>"$4""$URI".html</link>\
					<description>"$DESC"</description>\
					<image>"$4""$IMAGE"</image>\
					<pubDate>$(date -d "$DATE" '+%a, %d %b %Y 08:03:00 +0100')</pubDate>\
					<author>"$author"</author></item>" >> "$dst/feed.xml"
			esac
	done

	# close of the RSS file
	echo '</channel>' >>"$dst/feed.xml"
	echo '</rss>' >>"$dst/feed.xml"
}

render_html_file() {
	# h/t Devin Teske
	awk -v title="$1" '
	{ body = body "\n" $0 }
	END {
		body = substr(body, 2)
		if (body ~ /<\/?[Hh][Tt][Mm][Ll]/) {
			print body
			exit
		}
		if (match(body, /<[[:space:]]*[Hh]1(>|[[:space:]][^>]*>)/)) {
			t = substr(body, RSTART + RLENGTH)
			sub("<[[:space:]]*/[[:space:]]*[Hh]1.*", "", t)
			gsub(/^[[:space:]]*|[[:space:]]$/, "", t)
			if (t) title = t " &mdash; " title
		}
		n = split(ENVIRON["HEADER"], header, /\n/)
		for (i = 1; i <= n; i++) {
			if (match(tolower(header[i]), "<title></title>")) {
				head = substr(header[i], 1, RSTART - 1)
				tail = substr(header[i], RSTART + RLENGTH)
				print head "<title>" title "</title>" tail
			} else print header[i]
		}
		print body
		print ENVIRON["FOOTER"]
	}'
}


generate_post_list() {
	if [ -f "$1/index.html" ]; then
		rm $1/index*.html
	fi
	if [ -f "$1/posts.html" ]; then
		rm $1/posts.html
	fi
	while read -r f
	do
		if [ "$f" = "$src/index.html" ] || [ "$f" = "./whoami.md" ] || [ "$f" = "$src/posts.html" ] || [ "$f" = "./404.md"  ] || [ "$f" = "./contact.md" ]; then
			continue
		fi
		TITLE=$(grep -i title "$1/$f" | head -n1 | awk -F ":" '{ print $2 } ' | grep -Po '^.{1}\K.*' | sed 's/;$//')
		DATE=$(grep -i date "$1/$f" | head -n1 | awk -F ":" '{ print $2 }' | grep -Po '^.{1}\K.*' | sed 's/;$//')
		TAGS=$(grep -i tags "$1/$f" | head -n1 | awk -F ":" '{ print $2 }' | grep -Po '^.{1}\K.*' |  sed 's/;$//')
		DESC=$(grep -i desc "$1/$f" | head -n1 | awk -F ":" '{ print $2 }' | grep -Po '^.{1}\K.*' |  sed 's/;$//')
		printf "\
SortByDate:$DATE
[$DATE] <a href=\"/${f%\.md}.html\">$TITLE</a><br>\n\
$DESC<br><br>\n\

SortByDate\n\n" >> "$1/index.html"

		printf "\
SortByDate:$DATE
[$DATE] <a href=\"/${f%\.md}.html\">$TITLE</a></br>\n\
SortByDate\n\n" >> "$1/posts.html"
	done
}


list_pages() {
	e="\\( -name '*.html' -o -name '*.md' \\)"
	cd "$1" && eval "find . -type f ! -path '*/.*' ! -path '*/_*' $IGNORE $e" |	sed 's#^./##;s#.md$#.html#;s#/index.html$#/#'
}

render_sitemap() {
	urls="$1"
	base_url="$2"
	date="$3"

	echo '<?xml version="1.0" encoding="UTF-8"?>'
	echo '<urlset'
	echo 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"'
	echo 'xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9'
	echo 'http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"'
	echo 'xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'
	echo "$urls" |
		sed -E 's#^(.*)$#<url><loc>'"$base_url"'/\1</loc><lastmod>'"$date"'</lastmod><priority>1.0</priority></url>#'
	echo '</urlset>'
}

main "$@"n