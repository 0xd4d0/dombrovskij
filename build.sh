#!/bin/bash
# site information
dir_source="src"
dir_destination="dst"
site_name="Daniil Dombrovskij"
site_baseurl="https://dombrovskij.de"

# for the RSS feed
site_author="hello@dombrovskij.de"
site_description="learning how to break things"

# start timer for build time stat
timer_start=$(date +%s)

# remove old dir first in case this script is executed directly and not via 'make'
echo "[*] Removing old build"
rm -rf ./dst

echo "[*] Building"
mkdir $dir_destination
# bash ssg6 src dst title base_url author description
bash ssg6 "$dir_source" "$dir_destination" "$site_name" "$site_baseurl" "$site_author" "$site_description"

# generate toc function
# generatetoc(path/to/post.html)
generatetoc() {
	file="$1"
	# read all html lines
	while IFS= read -r line
	do 
		# only get lines that contain '<h* id=' and are only h2,h3,h4
		if [[ $line =~ ^\<h[2-4].id= ]]
		then
			# get h*
			tocdepth=$(echo $line | awk -F"id=" '{print $1}' | sed 's|<||g' | sed "s/ //g")
			# cut off by id= and > and remove '"'
			bookmark_link=$(echo $line | awk -F"id=" '{print $2}' | awk -F">" '{print $1}' | sed 's|"||g')
			# name: cut off by id= and > and remove '", replace - with spaces
			bookmark_name=$(echo $line | awk -F"id=" '{print $2}' | awk -F">" '{print $1}' | sed 's|"||g' | sed 's|-| |g')

			# switchcase for h1 - h5
			case $tocdepth in

				h1)
				# CURRENTLY ignored by regex at the start
				# return html anchor
				echo "\•&nbsp;<a href=\"#""$bookmark_link""\">""$bookmark_name""</a><br>"
				;;

				h2)
				# return html anchor
				echo "\•&nbsp;<a href=\"#""$bookmark_link""\">""$bookmark_name""</a><br>"
				;;

				h3)
				# return html anchor
				echo "&nbsp;&nbsp;\•&nbsp;<a href=\"#""$bookmark_link""\">""$bookmark_name""</a><br>"
				;;

				h4)
				# return html anchor
				echo "&nbsp;&nbsp;&nbsp;&nbsp;\•&nbsp;<a href=\"#""$bookmark_link""\">""$bookmark_name""</a><br>"
				;;

				h5)
				# CURRENTLY ignored by regex at the start
				# return html anchor
				echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\•&nbsp;<a href=\"#""$bookmark_link""\">""$bookmark_name""</a><br>"
				;;

				*)
				# some error
				# return html anchor
				echo "<a href=\"#""$bookmark_link""\"> INDEXERROR ON ""$bookmark_name""</a><br>"
				;;

			esac
		fi
	done < $file
}

# generate back to the top function
# generatebttt(path/to/post.html)
generatebttt() {
	file="$1"
	# read all html lines
	while IFS= read -r line
	do 
		# only get lines that contain '<h1'
		if [[ $line =~ ^\<h[1].id= ]]
		then
			# cut off by id= and > and remove '"'
			bookmark_link=$(echo $line | awk -F"id=" '{print $2}' | awk -F">" '{print $1}' | sed 's|"||g')
			echo "<bttn>[<a href=\"#""$bookmark_link""\">back to the top</a>]</bttn><br>"
		fi
	done < $file
}

# for each post
echo "[*] Modifying blog posts"
for i in $(find $dir_destination/posts -type f -name "*.html") 
do
	# get md filename from html filename
	mdfile=$(echo "$dir_source""/posts/"$(echo $i | awk -F"/" '{print $3}' | sed 's|.html||g')".md")

	# get heading from md file
	heading=$(cat $mdfile | grep 'title: ' | awk -F": " '{print $2}' | sed 's|"||g')
	# for the html bookmark replace spaces with -
	heading_nospaces=$(echo $heading | sed "s/ /-/g")
	# add html formatting (with escape chars)
	headingline=("\<h1 id=\"$heading_nospaces\">$heading\<\/h1>")
	# the line number after <article> + 1 = line for heading
	linenumber_heading=($(($(cat $i  | grep -Fn "<article>" | awk -F":" '{print $1}')+1))"i")
	# insert heading
	sed -i "$linenumber_heading$headingline" $i

	# add post title to the title tag 
	sed -i "s/\<title>/title>$heading \&mdash; /" $i

	# get date from md file
	date=$(cat $mdfile | grep 'date: ' | awk -F": " '{print $2}' | sed 's|"||g')
	# add html formatting (with escape chars)
	dateline=("\<p>[""$date""]\<\/p>")
	# the line number after <article> + 2 = line for date
	linenumber_date=($(($(cat $i | grep -Fn '<article>' | awk -F":" '{print $1}')+2))"i")
	# insert date
	sed -i "$linenumber_date$dateline" $i

	# generate toc
	toc=$(generatetoc $i)
	# escape all the '<'
	toc=$(echo $toc | sed -e "s/</\\\</g")
	# add one more <br> at the end (with escape chars)
	toc="$toc\<br>"
	# the line number after <article> + 3 = line for toc
	linenumber_toc=($(($(cat $i | grep -Fn '<article>' | awk -F":" '{print $1}')+3))"i")
	# insert toc
	sed -i "$linenumber_toc$toc" $i

	# add back to the top at bottom
	bttt=$(generatebttt $i)
	# escape all the '<'
	bttt=$(echo $bttt | sed -e "s/</\\\</g")
	# the line number after </article> + 1 = line for bttt
	linenumber_bttt=($(($(cat $i | grep -Fn '</article>' | awk -F":" '{print $1}')+3))"i")
	# insert bttt
	sed -i "$linenumber_bttt$bttt" $i
done

# add stats
echo "[*] Searching and replacing placeholders"
# how long ssg took to generate the site
runtime=$(($(date +%s)-timer_start))
# how many file are in the final dir (excluding dst/img/)
currentfiles=$(find "$dir_destination" -type f | grep -v "dst/img" | wc -l)
# how large is the final dir (excluding dst/img/)
currentsizeall=$(du -h -c "$dir_destination" | grep total | awk -F" " '{print $1}')
currentsizemedia=$(du -h -c "$dir_destination/img" | grep total | awk -F" " '{print $1}')
# remove last char "M" from strings, do math, add M again
# math from https://stackoverflow.com/a/24656653
currentsize="$(echo ${currentsizeall%?} ${currentsizemedia%?} | awk '{print $1-$2}')M"

# check all html files for variables to replace
for i in $(find "$dir_destination" -type f -name "*.html") 
do 
	sed -i "s/REPLACEBUILD/$timer_start/" $i
	sed -i "s/REPLACEBUILDTIME/$runtime/" $i
	# CURRENTLY not used
	#sed -i "s/REPLACEFILES/$currentfiles/" $i
	# CURRENTLY not used
	#sed -i "s/REPLACESIZE/$currentsize/" $i
done

rm -rf $dir_destination/.files
echo "[+] Done"
